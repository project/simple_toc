<?php

/**
 * @file
 * A Simple TOC (Table of Contents) generated from node text areas.
 *
 * Simple TOC utilises cache to reduce the amount of processing for node fields.
 * For simplicity, it uses it's own cache table, 'cache_simple_toc'
 * Cache ID's ($cids) are set as 'type:nid'. This allows for easy and specific
 * cache flushing, as required.
 *
 */

/**
 * Implements hook_form_alter().
 */
function simple_toc_form_field_ui_field_edit_form_alter(&$form, $form_state, $form_id) {
  // Add a note below cardinality setting for text fields.
  if (($form['#field']['type'] == 'text_with_summary' || $form['#field']['type'] == 'text_long') && isset($form['field']['cardinality'])) {
    $info = "<br/><br/>To use this text field with Simple TOC, this must be set to 1.<br/>";
    $info .= "Updating this value to a value other than 1 will remove any existing Simple TOC settings related to this text field.<br/>";
    $info .= "Simple TOC settings are adjusted per node type, on the node type edit form, eg ";
    $info = t($info);
    $info .= l(
      t("Node Type @type Edit", array('@type' => ucfirst($form['#instance']['bundle']))),
      "admin/structure/types/manage/{$form['#instance']['bundle']}"
      );
    $form['field']['cardinality']['#description'] .= $info;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function simple_toc_form_node_type_form_alter(&$form, $form_state, $form_id) {

  if (isset($form['#node_type'])) {

    // Build a list of fields that are suitable for TOC.
    $options = array();
    $fields_info = field_info_instances('node', $form['#node_type']->type);
    foreach ($fields_info as $field_name => $value) {
      $field_info = field_info_field($field_name);
      if (($field_info['type'] == 'text_with_summary' || $field_info['type'] == 'text_long') && $field_info['cardinality'] == '1') {
        $options[$field_info['field_name']] = $value['label'];
      }
    }

    $settings = variable_get('simple_toc');

    $form['simple_toc'] = array(
      '#type' => 'fieldset',
      '#title' => t('Simple TOC settings'),
      '#collapsible' => TRUE,
      '#group' => 'additional_settings',
    );

    $form['simple_toc']['simple_toc_field'] = array(
      '#type' => 'select',
      '#title' => t('Enable TOC for a field.'),
      '#description' => t('Perform TOC modifications to the selected single-value Long-Text field.'),
      '#options' => $options,
      '#empty_value' => '',
      '#weight' => 0,
      '#default_value' => variable_get('simple_toc_field_' . $form['#node_type']->type),
    );

    if (isset($form['#node_type']->is_new)) {
      $form['simple_toc']['simple_toc_field']['#description'] = t('This is a new content type and no fields are available yet.
       Please come back to this form after a single-value Long-Text field has been created.');
      return;
    }

    $form['simple_toc']['advanced'] = array(
      '#type' => 'fieldset',
      '#title' => t("Advanced settings"),
      '#states' => array('invisible' => array(':input[name="simple_toc_field"]' => array('value' => ''))),
      '#weight' => 10,
    );

    $form['simple_toc']['advanced']['simple_toc_attach'] = array(
      '#type' => 'select',
      '#title' => t('Optionally attach the TOC to the selected node field.'),
      '#description' => t('The TOC can also be displayed seperately in a block, Display-Suite field or Content pane.'),
      '#options' => array('1' => 'Before', '2' => 'After', '3' => 'Both'),
      '#empty_value' => '0',
      '#weight' => 0,
      '#default_value' => variable_get('simple_toc_attach_' . $form['#node_type']->type),
    );

    $form['simple_toc']['advanced']['list'] = array(
      '#type' => 'fieldset',
      '#title' => t("TOC List Options"),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 5,
    );

    $form['simple_toc']['advanced']['list']['simple_toc_type'] = array(
      '#type' => 'select',
      '#title' => t('Output the TOC formated as...'),
      '#options' => array('ol' => 'Organised List', 'ul' => 'Unorganised List', 'div' => 'Unformatted divs'),
      '#weight' => 0,
      '#default_value' => variable_get('simple_toc_type_' . $form['#node_type']->type),
    );

    if (module_exists('ctools')) {
      $form['simple_toc']['advanced']['list']['simple_toc_collapsible'] = array(
        '#type' => 'select',
        '#title' => t('Make the TOC Collapsible?'),
        '#description' => t('The TOC Title Text will be used for the collapsible header. Leverages Ctools.'),
        '#options' => array('no' => 'No', FALSE => 'Yes - Open', TRUE => 'Yes - Collapsed'),
        '#weight' => 1,
        '#default_value' => variable_get('simple_toc_collapsible_' . $form['#node_type']->type),
      );
    }

    $form['simple_toc']['advanced']['list']['simple_toc_title_type'] = array(
      '#type' => 'select',
      '#title' => t('Output a title for the TOC formatted as...'),
      '#description' => t('The TOC Title will appear before the TOC. If you make the TOC Collapsible,
       or display the TOC in a Block/DS-Field/Pane, you could have multiple TOC titles.'),
      '#options' => array('h1' => 'H1', 'h2' => 'H2', 'h3' => 'H3', 'h4' => 'H4',
      'h5' => 'H5', 'h6' => 'H6', 'div' => 'DIV', 'span' => 'SPAN'),
      '#weight' => 5,
      '#empty_value' => '',
      '#default_value' => variable_get('simple_toc_title_type_' . $form['#node_type']->type),
    );

    $form['simple_toc']['advanced']['list']['simple_toc_title_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Change the text inserted as the TOC Title.'),
      '#description' => t('The title text will also be used as the Collapsible TOC header,
       the manual TOC title and the default Block/Pane title.'),
      '#weight' => 10,
      '#default_value' => variable_get('simple_toc_title_text_' . $form['#node_type']->type, 'Table of Contents'),
    );

    $form['simple_toc']['advanced']['top'] = array(
      '#type' => 'fieldset',
      '#title' => t("'Back to Top' Link Options"),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 10,
    );

    $form['simple_toc']['advanced']['top']['simple_toc_top_insert'] = array(
      '#type' => 'select',
      '#title' => t('Insert "Back to Top" links within the content following a header.'),
      '#options' => array('1' => 'Before', '2' => 'After', '3' => 'Both'),
      '#empty_value' => '0',
      '#weight' => 0,
      '#default_value' => variable_get('simple_toc_top_insert_' . $form['#node_type']->type),
    );

    $form['simple_toc']['advanced']['top']['simple_toc_top_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Change the text inserted for the "Back to Top" links.'),
      '#weight' => 5,
      '#default_value' => variable_get('simple_toc_top_text_' . $form['#node_type']->type, 'Back to Top'),
    );

    $form['simple_toc']['advanced']['anchor'] = array(
      '#type' => 'fieldset',
      '#title' => t("Anchor Link Options"),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 15,
    );

    $form['simple_toc']['advanced']['anchor']['simple_toc_anchor'] = array(
      '#type' => 'textfield',
      '#title' => t('Change the text used to create the anchors for the headings.'),
      '#description' => t('The string should contain (toc), which will be replaced with the relevant anchor number.
       Node tokens are available. For valid HTML, string should only contain letters (A-Z,a-z), digits (0-9),
        hyphens (-), underscores (_), colons (:), and periods (.).'),
      '#weight' => 0,
      '#default_value' => variable_get('simple_toc_anchor_' . $form['#node_type']->type, 'sect(toc)'),
    );

    if (module_exists('token')) {
      $form['simple_toc']['advanced']['anchor']['tokens'] = array(
        '#theme' => 'token_tree',
        '#token_types' => array('node'),
        '#weight' => 5,
      );
    }
  }
}

/**
 * Implements hook_node_load().
 */
function simple_toc_node_load($nodes, $types) {

  // Do the node processing in another function, so it may be called elsewhere.
  simple_toc_node_handler($nodes, $types);

}

/**
 * A function to handle nodes supplied predominantly from hook_node_load().
 *
 * @param array $nodes
 *   An array of nodes with the key set to nid.
 *
 * @param array $types
 *   An optional array of node types present in $nodes.
 */
function simple_toc_node_handler($nodes, $types = array()) {

  // Ensure $types is populated.
  if (empty($types)) {
    foreach ($nodes as $nid => $node) {
      $types[] = $node->type;
    }
  }

  // Build a list of types that need processing.
  $types_to_process = array();
  foreach ($types as $type) {
    if (variable_get('simple_toc_field_' . $type)) {
      $types_to_process[$type] = $type;
    }
  }

  // If there are no types to process, there is nothing to do.
  if (empty($types_to_process)) {
    return;
  }

  // Build a list of nodes that need processing.
  $cids_to_process = array();
  foreach ($nodes as $nid => $node) {
    if (in_array($node->type, $types_to_process)) {
      $cids_to_process[$node->type . ':' . $nid] = array(
        'type' => $node->type,
        'nid' => $nid,
        );
    }
  }

  // If there are no nodes to process, there is nothing to do.
  if (empty($cids_to_process)) {
    return;
  }

  // Get any available static data.
  $static_data = &drupal_static(__FUNCTION__, array());

  // For nids not in static data, attempt to load cache data into static data.
  if ($get_cids_from_cache = array_diff_key($cids_to_process, $static_data)) {

    // cache_get_multiple() needs an array where the values are the cids.
    $get_cids_from_cache_values = array_keys($get_cids_from_cache);
    $cache_data = cache_get_multiple($get_cids_from_cache_values, 'cache_simple_toc');

    // Parse cache data and add to static data.
    foreach ($cache_data as $cache_item) {
      $static_data[$cache_item->cid] = $cache_item->data;
    }

    // cache_get_multiple removes items from the passed array that have cache,
    // leaving only those that do not have cache data and need TOC building.
    $cids_to_build_toc = array_intersect_key($cids_to_process, array_flip($get_cids_from_cache_values));
  }

  // Build TOC for any nids that require it.
  if (!empty($cids_to_build_toc)) {
    // Iterate through each nid.
    foreach ($cids_to_build_toc as $cid => $cid_values) {
      // Get settings for the node's type.
      $type = $cid_values['type'];
      $nid = $cid_values['nid'];
      $settings['node_field'] = variable_get('simple_toc_field_' . $type);
      $settings['attach'] = variable_get('simple_toc_attach_' . $type, 0);
      $settings['toc_type'] = variable_get('simple_toc_type_' . $type);
      $settings['collapsible'] = variable_get('simple_toc_collapsible_' . $type);
      $settings['title_type'] = variable_get('simple_toc_title_type_' . $type);
      $settings['title_text'] = check_plain(variable_get('simple_toc_title_text_' . $type));
      $settings['anchor_pre'] = check_plain(variable_get('simple_toc_anchor_' . $type));
      $settings['top_insert'] = variable_get('simple_toc_top_insert_' . $type);
      $settings['top_text'] = check_plain(variable_get('simple_toc_top_text_' . $type));

      // Process the field (if it has data, otherwise empty defaults will be used).
      $field_name = $settings['node_field'];
      $language = field_language('node', $nodes[$nid], $field_name);

      // Set the defaults.
      $static_data[$cid] = array(
        'safe_value' => '',
        'toc' => array(),
        'field' => $field_name,
        'type' => $type,
        'vid' => $nodes[$nid]->vid,
        'language' => $language,
        );

      // Process fields that aren't empty (empty fields stay as empty defaults).
      if (isset($nodes[$nid]->$field_name) && ($field = $nodes[$nid]->$field_name) && ($field_value = $field[$language][0]['safe_value'])) {
        // Build new anchor with tokens, ready for TOC build.
        $settings['anchor_post'] = token_replace($settings['anchor_pre'], array('node' => $nodes[$nid]));
        // Build a TOC from the field.
        $toc = simple_toc_build_toc($field_value, $settings);
        // Set the TOC into static data.
        $static_data[$cid]['safe_value'] = $toc['html'];
        $static_data[$cid]['toc'] = array(
          'value' => $toc['toc'],
          'title' => t($settings['title_text']),
          'access' => FALSE,
          );
      }

      // Save the TOC data into cache.
      cache_set($cid, $static_data[$cid], 'cache_simple_toc');
    }
  }

  // All processed fields and TOC should now be in static data,
  // so replace unprocessed node data with new data.
  foreach ($cids_to_process as $cid => $cid_values) {
    $type = $cid_values['type'];
    $nid = $cid_values['nid'];
    $field_name = $static_data[$cid]['field'];
    $language = $static_data[$cid]['language'];
    if (isset($nodes[$nid]->$field_name) && ($field = &$nodes[$nid]->$field_name)) {
      $field[$language][0]['safe_value'] = $static_data[$cid]['safe_value'];
    }
    $nodes[$nid]->simple_toc = $static_data[$cid]['toc'];
  }

}

/**
 * Generates a TOC from a HTML String, generally the nodes body.
 *
 * @param string $html
 *   HTML string to be processed, such as the node bodies safe_value.
 *
 * @param array $settings
 *   Settings to be used to process the field.
 *   ['node_field'] = The field of the node type.
 *   ['attach']     = Attach the TOC to the HTML string.
 *                      Valid settings 1=Before, 2=After, 3=Both.
 *   ['toc_type']   = The type of TOC to build. Valid settings ol, ul, div.
 *   ['collapsible']= Put the TOC in a collapsible div provided by Ctools (Yes/No).
 *   ['title_type'] = The HTML element for the TOC title.
 *   ['title_text'] = The text to use for the TOC title.
 *   ['anchor_pre'] = Anchor text with tokens un-processed. NOT USED BY THIS FUNCTION.
 *   ['anchor_post']= Anchor text with tokens processed.
 *   ['top_insert'] = Insert 'Back to Top' links into the content following a header.
 *                      Valid settings 1=Before, 2=After, 3=Both.
 *   ['top_text']   = The text to use for 'Back to Top' links if they are inserted.
 *
 * @return array
 *   - If headings do not exist in $html, returns false.
 *   - If headings exist in $html, array returned containing;
 *   ['html'] = The processed $html string.
 *   ['toc'] = The TOC.
 *
 * @TODO
 *   DOMDocument error reporting has been disabled to handle HTML5 elements.
 *   One day these might be handled by DOMDocument and errors can be enabled.
 *   See http://drupal.org/node/1964578
 */
function simple_toc_build_toc($html, $settings) {

  // If the html does not contain headings, return nothing.
  if (preg_match('!<h[1-6].*>.*</h[1-6]>!', $html) == FALSE) return FALSE;

  // Create a DOMDocument and populate it with the supplied HTML.
  $dom_html = new TocDOMDocument();
  libxml_use_internal_errors(true);
  // Encoding the HTML as UTF-8 - in order to handle special characters.
  $dom_html->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
  libxml_clear_errors();
  $xpath = new DOMXPath($dom_html);
  $last = 1;

  // Create a DOMDocument for the TOC.
  $dom_toc = new TocDOMDocument();

  // Create a div to wrap the entire TOC.
  $elmnt = $dom_toc->createElement('div');
  $elmnt->setAttribute('class', 'toc-wrapper');
  $dom_toc->appendChild($elmnt);

  // Add a title to the TOC, if configured.
  if ($settings['title_type']) {
    $elmnt = $dom_toc->createElement($settings['title_type'], t($settings['title_text']));
    $elmnt->setAttribute('class', 'toc-title');
    $dom_toc->firstChild->appendChild($elmnt);
  }

  // Create a top TOC element, and set the head to it.
  $elmnt = $dom_toc->createElement($settings['toc_type']);
  $elmnt->setAttribute('class', 'toc');
  $dom_toc->firstChild->appendChild($elmnt);
  $head = &$dom_toc->firstChild->lastChild;

  // Get all H1, H2, …, H6 elements from the HTML.
  foreach ($xpath->query('//*[self::h1 or self::h2 or self::h3 or self::h4 or self::h5 or self::h6]') as $headline) {

    // Get level of current headline.
    sscanf($headline->tagName, 'h%u', $curr);

    // Move head reference if necessary.
    if ($curr < $last) {
      // Move upwards.
      for ($i = $curr; $i < $last; $i++) {
        $head = &$head->parentNode->parentNode;
      }
    }
    elseif ($curr > $last && $head->lastChild) {
      // Move downwards and create new sub-toc wrappers.
      for ($i = $last; $i < $curr; $i++) {
        $elmnt = $dom_toc->createElement($settings['toc_type']);
        $elmnt->setAttribute('class', 'indented toc-h' . $curr . '-wrap');
        $head->lastChild->appendChild($elmnt);
        $head = &$head->lastChild->lastChild;
      }
    }
    $last = $curr;

    // Add the TOC item to its wrapper.
    if ($settings['toc_type'] == 'div') {
      $elmnt = $dom_toc->createElement('div');
    }
    else {
      $elmnt = $dom_toc->createElement('li');
    }
    $elmnt->setAttribute('class', 'toc-h' . $last);
    $head->appendChild($elmnt);

    // Build ID.
    $levels = array();
    $tmp = &$head;
    while (!is_null($tmp) && $tmp != $dom_toc) {
      // Walk subtree up to fragment root node of this subtree.
      $levels[] = $tmp->childNodes->length;
      $tmp = &$tmp->parentNode->parentNode;
    }
    $id_num = implode('.', array_reverse($levels));
    $id = str_replace('(toc)', $id_num, $settings['anchor_post'], $replace_count);
    // If (toc) was not in the string and id number could not be added, stick it on the end.
    if ($replace_count == 0) $id .= $id_num;

    // Create a link for the TOC.
    $elmnt = $dom_toc->createElement('a', $headline->textContent);
    $elmnt->setAttribute('href', '#' . $id);
    $elmnt->setAttribute('class', 'toc-h' . $last);
    $head->lastChild->appendChild($elmnt);

    // Add anchor inside the header. Cannot add ID to the header as it may already have an ID.
    $elmnt = $dom_html->createElement('a', '');
    $elmnt->setAttribute('id', $id);
    $headline->insertBefore($elmnt, $headline->firstChild);

    // Add back to top link before and/or after content following headline.
    $top_text = t($settings['top_text']);
    if ($settings['top_insert'] == 1 || $settings['top_insert'] == 3) {
      $elmnt = $dom_html->createElement('a', $top_text);
      $elmnt->setAttribute('href', '#' . $settings['node_field'] . '-top');
      $elmnt->setAttribute('class', 'top-link top-link-before toc-top-h' . $last);
      $headline->parentNode->insertBefore($elmnt, $headline->nextSibling);
    }
    if ($settings['top_insert'] == 2 || $settings['top_insert'] == 3) {
      $elmnt = $dom_html->createElement('a', $top_text);
      $elmnt->setAttribute('href', '#' . $settings['node_field'] . '-top');
      $elmnt->setAttribute('class', 'top-link top-link-after toc-top-h' . $last);
      $headline->parentNode->insertBefore($elmnt, $headline->previousSibling);
    }
  }

  // If 'Back to Top' links were inserted, add an anchor to the beginning.
  if ($settings['top_insert'] == 1 || $settings['top_insert'] == 2 || $settings['top_insert'] == 3) {
    $elmnt = $dom_html->createElement('a', '');
    $elmnt->setAttribute('id', $settings['node_field'] . '-top');
    $dom_html->insertBefore($elmnt, $dom_html->firstChild);
  }

  // DOMdocument modifications are now complete.

  // Save the dom_html as the HTML to return.
  $markup = $dom_html->saveHTML();
  $markup = preg_replace('~<(?:!DOCTYPE|/?(?:html|head|body))[^>]*>\s*~i', '', $markup);
  $return['html'] = $markup;

  // Save the dom_toc as the TOC to return.
  $toc = $dom_toc->saveHTML();
  $toc = preg_replace('~<(?:!DOCTYPE|/?(?:html|head|body))[^>]*>\s*~i', '', $toc);
  $return['toc'] = $toc;

  // Make the TOC collapsible, if required.
  if (module_exists('ctools') && $settings['collapsible'] != 'no') {
    $return['toc'] = theme('ctools_collapsible', array(
      'handle' => t($settings['title_text']),
      'content' => $return['toc'],
      'collapsed' => $settings['collapsible'],
      )
    );
  }

  // Attach the TOC to the start/end of HTML, if required.
  if ($settings['attach'] == 1 || $settings['attach'] == 3) {
    $return['html'] = $return['toc'] . $return['html'];
  }
  if ($settings['attach'] == 2 || $settings['attach'] == 3) {
    $return['html'] .= $return['toc'];
  }

  return $return;

}

/**
 * Allows characters to be used in created elements.
 *
 * Characters such as & when used in headers will cause the error
 * "Warning: DOMDocument::createElement(): unterminated entity reference".
 * The extended classes are a safer alternative to the (possibly inconsistent)
 * use of htmlentities(), as documented in the first link.
 *
 * @see http://www.php.net/manual/en/domdocument.createelement.php#73617
 * @see http://drupal.org/node/1955782
 */
class TocDOMElement extends DOMElement {
  /* Refer information above class TocDOMElement. */
  function __construct($name, $value = NULL, $namespaceURI = NULL) {
    parent::__construct($name, NULL, $namespaceURI);
  }
}

/* Refer information above class TocDOMElement. */
class TocDOMDocument extends DOMDocument {
  /* Refer information above class TocDOMElement. */
  function __construct($version = NULL, $encoding = NULL) {
    parent::__construct($version, $encoding);
    $this->registerNodeClass('DOMElement', 'TocDOMElement');
  }
  /* Refer information above class TocDOMElement. */
  function createElement($name, $value = NULL, $namespaceURI = NULL) {
    $element = new TocDOMElement($name, $value, $namespaceURI);
    $element = $this->importNode($element);
    if (!empty($value)) {
      $element->appendChild(new DOMText($value));
    }
    return $element;
  }
}

/**
 * Implements hook_flush_caches().
 */
function simple_toc_flush_caches() {
  // Flush Simple TOC cache during flush all caches.
  return array('cache_simple_toc');
}

/**
 * Implements hook_field_update_field().
 */
function simple_toc_field_update_field($field, $prior_field, $has_data) {
  // If the cardinality of a Simple TOC enabled field is changed,
  // Simple TOC will be disabled for the node type, and a message displayed.
  if ($field['cardinality'] != '1') {

    $types_disabled = array();

    // Parse each node type that uses this field.
    foreach ($field['bundles']['node'] as $type) {
      // Check if this field is used as the TOC field for each node type.
      if (variable_get("simple_toc_field_{$type}", FALSE) == $field['field_name']) {
        // Disable the variable, and add the node type to a list for later use.
        variable_del("simple_toc_field_{$type}");
        $types_disabled[] = $type;
      }
    }

    // Set status message informing the user which nodes Simple TOC was disabled on.
    if (!empty($types_disabled)) {
      $types_disabled = implode(", ", $field['bundles']['node']);
      drupal_set_message(
        t("Due to change in cardinality setting for field: @field_name, Simple TOC has been disabled for the following node types: @types_disabled.",
          array('@field_name' => $field['field_name'], '@types_disabled' => $types_disabled))
        , 'warning');
    }
  }

}

/**
 * Implements hook_node_update().
 */
function simple_toc_node_update($node) {
  // Clear the cache entry for this node.
  cache_clear_all($node->type . ':' . $node->nid, 'cache_simple_toc');
}

/**
 * Implements hook_node_delete().
 */
function simple_toc_node_delete($node) {
  // Clear the cache entry for this node.
  cache_clear_all($node->type . ':' . $node->nid, 'cache_simple_toc');
}

/**
 * Implements hook_node_type_update().
 */
function simple_toc_node_type_update($info) {
  // Clear all cache entries for nodes of this type.
  cache_clear_all($info->type . ':', 'cache_simple_toc', TRUE);
}

/**
 * Implements hook_node_type_delete().
 */
function simple_toc_node_type_delete($info) {
  // Clear all cache entries for nodes of this type.
  cache_clear_all($info->type . ':', 'cache_simple_toc', TRUE);

  // Also delete any variables associated with the type.
  if ($vars = db_select('variable')->condition('name', db_like('simple_toc_') . '%' . $info->type, 'LIKE')->execute()) {
    foreach ($vars as $var) {
      variable_del($var);
    }
  }
}

/**
 * Implements hook_filter_format_update().
 */
function simple_toc_filter_format_update($format) {
  // An updated text format could alter any and all fields that TOC processes,
  // hence the entire cache needs to be flushed for safety.
  cache_clear_all('*', 'cache_simple_toc', TRUE);
}

/**
 * Implements hook_block_info().
 */
function simple_toc_block_info() {
  $blocks['simple_toc'] = array(
    'info' => t('Simple TOC'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function simple_toc_block_view($delta) {
  if (($node = menu_get_object()) && isset($node->simple_toc)) {
    $block = array();
    $block['subject'] = $node->simple_toc['title'];
    $block['content'] = $node->simple_toc['value'];
    return $block;
  }
}

/**
 * Sets up the Ctools plugin.
 */
function simple_toc_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'ctools' && $plugin_type == 'content_types') {
    return 'plugins/content_types';
  }
}

/**
 * Implements hook_ds_fields_info().
 */
function simple_toc_ds_fields_info($entity_type) {
  $fields = array();

  $fields['node']['ds_simple_toc'] = array(
    'title' => t('Table of Contents'),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'function' => 'simple_toc_ds_fields_render',
  );

  if (isset($fields[$entity_type])) {
    return array($entity_type => $fields[$entity_type]);
  }
  return;
}

/**
 * Renders DisplaySuite fields.
 */
function simple_toc_ds_fields_render(&$vars) {
  if (isset($vars['entity']->simple_toc['value'])) {
    return $vars['entity']->simple_toc['value'];
  }
}
